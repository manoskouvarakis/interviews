﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class StoiximanCodilityTest
	{
		public String FindBug(String S)
		{
			int[] occurrences = new int[26];
			foreach (char ch in S)
			{
				occurrences[(int)ch - 'a']++;
			}

			char best_char = 'a';
			int best_res = 0;

			for (int i = 0; i < 26; i++)
			{
				if (occurrences[i] > best_res)
				{
					best_char = (char)('a' + i);
					best_res = occurrences[i];
				}
			}

			return best_char.ToString();
		}

		public int[] AllNumbersSumZero(int N)
		{
			int[] result = new int[N];
			int index = 0;

			for (int i = (-N / 2); i < (N / 2) + 1; i++)
			{
				if (i == 0) continue;

				result[index++] = i;
			}

			return result;
		}


		private static readonly int MUSIC = 0;
		private static readonly int IMAGE = 1;
		private static readonly int MOVIE = 2;
		private static readonly int OTHER = 3;

		private Dictionary<String, int> mExtensionTypes = null;
		public Dictionary<String, int> ExtensionTypes
		{
			get
			{
				if (this.mExtensionTypes == null)
				{
					mExtensionTypes = new Dictionary<string, int>
				{
					{ "mp3", MUSIC },
					{ "aac", MUSIC },
					{ "flac", MUSIC },
					{ "jpg", IMAGE },
					{ "bmp", IMAGE },
					{ "gif", IMAGE },
					{ "mp4", MOVIE },
					{ "avi", MOVIE },
					{ "mkv", MOVIE }
				};
				}
				return this.mExtensionTypes;
			}
		}

		public string DiskStatistics(string S)
		{
			int[] typeBytes = new int[4];

			StringReader strReader = new StringReader(S);
			while (true)
			{
				String line = strReader.ReadLine();
				if (line == null) break;

				string[] splittedWords = line.Split(' ');
				String fileName = splittedWords[0];
				String fileSize = splittedWords[1];

				string[] tokens = fileName.Split('.');
				String extension = tokens.Last();

				int bytes = 0;
				int lastByteCharIndex = fileSize.LastIndexOf('b');
				if (lastByteCharIndex == -1)
				{
					bytes = Convert.ToInt32(fileSize);
				}
				else
				{
					String bytesString = fileSize.Remove(lastByteCharIndex, 1);
					bytes = Convert.ToInt32(bytesString);
				}

				if (this.ExtensionTypes.ContainsKey(extension))
					typeBytes[this.ExtensionTypes[extension]] += bytes;
				else
					typeBytes[OTHER] += bytes;
			}

			StringBuilder sb = new StringBuilder();
			sb.AppendLine(String.Format("music {0}b", typeBytes[MUSIC].ToString()));
			sb.AppendLine(String.Format("images {0}b", typeBytes[IMAGE].ToString()));
			sb.AppendLine(String.Format("movies {0}b", typeBytes[MOVIE].ToString()));
			sb.Append(String.Format("other {0}b", typeBytes[OTHER].ToString()));

			return sb.ToString();
		}
	}
}
