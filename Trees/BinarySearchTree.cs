﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class BinarySearchTree
	{
		public class Node
		{
			public int Value { get; set; }
			public Node Left { get; set; }
			public Node Right { get; set; }
		}

		private Node Root { get; set; }

		//Lookup Recursive
		public Node LookUp(int value)
		{
			return this.LookUp(this.Root, value);
		}

		private Node LookUp(Node root, int value)
		{
			if (root == null) return null;
			if (root.Value == value) return root;

			if (root.Value > value) return this.LookUp(root.Left, value);
			else if (root.Value < value) return this.LookUp(root.Right, value);

			return null;
		}

		public Node GetMinNode()
		{
			if (this.Root == null) return null;

			Node currentNode = this.Root;
			while (currentNode.Left != null)
			{
				currentNode = currentNode.Left;
			}
			return currentNode;
		}

		public Node GetMaxNode()
		{
			if (this.Root == null) return null;

			Node currentNode = this.Root;
			while (currentNode.Right != null)
			{
				currentNode = currentNode.Right;
			}
			return currentNode;
		}

		public Node GetLowestCommonAnsestor(int value1, int value2)
		{
			Node node = this.Root;

			while (node != null)
			{
				if (node.Value > value1 && node.Value > value2)
					node = node.Left;
				else if (node.Value < value1 && node.Value < value2)
					node = node.Right;
				else return node;
			}
			return null;
		}

		public Node GetLowestCommonAnsestor(Node node, int value1, int value2)
		{
			if (node == null) return null;

			if (node.Value > value1 && node.Value > value2) return this.GetLowestCommonAnsestor(node.Left, value1, value2);
			if (node.Value < value1 && node.Value < value2) return this.GetLowestCommonAnsestor(node.Right, value1, value2);

			return node;
		}
	}
}
