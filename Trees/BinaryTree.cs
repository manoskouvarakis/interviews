﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class BinaryTree
	{
		public class Node
		{
			public int Value { get; set; }
			public Node Left { get; set; }
			public Node Right { get; set; }
		}

		public Node Root { get; set; }

		public BinaryTree()
		{
			this.Root = new BinaryTree.Node();
			this.Root.Value = 1;

			BinaryTree.Node temp2 = new BinaryTree.Node();
			temp2.Value = 2;
			BinaryTree.Node temp3 = new BinaryTree.Node();
			temp3.Value = 3;
			BinaryTree.Node temp4 = new BinaryTree.Node();
			temp4.Value = 4;
			BinaryTree.Node temp5 = new BinaryTree.Node();
			temp5.Value = 5;
			BinaryTree.Node temp6 = new BinaryTree.Node();
			temp6.Value = 6;
			BinaryTree.Node temp7 = new BinaryTree.Node();
			temp7.Value = 7;

			temp2.Left = temp4;
			temp2.Right = temp5;

			temp3.Left = temp6;
			temp3.Right = temp7;

			this.Root.Left = temp2;
			this.Root.Right = temp3;
		}

		#region BFS

		public void BFS()
		{
			if (this.Root == null) return;

			Queue<Node> queue = new Queue<Node>();
			queue.Enqueue(this.Root);

			while (queue.Count > 0)
			{
				Node temp = queue.Dequeue();
				Console.WriteLine(temp.Value);

				if (temp.Left != null) queue.Enqueue(temp.Left);
				if (temp.Right != null) queue.Enqueue(temp.Right);
			}
		}

		#endregion

		#region DFS

		public void DFSPreOrder()
		{
			DFSPreOrder(this.Root);
		}

		public void DFSPreOrder(Node node)
		{
			if (node == null) return;

			Console.WriteLine(node.Value);
			if (node.Left != null) DFSPreOrder(node.Left);
			if (node.Right != null) DFSPreOrder(node.Right);
		}

		public void DFSPreOrderNoRecursion()
		{
			if (this.Root == null) return;

			Stack<Node> stack = new Stack<Node>();
			stack.Push(this.Root);

			while (stack.Count > 0)
			{
				Node temp = stack.Pop();
				Console.WriteLine(temp.Value);

				if (temp.Right != null) stack.Push(temp.Right);
				if (temp.Left != null) stack.Push(temp.Left);
			}
		}

		public void DFSInOrder()
		{
			DFSInOrder(this.Root);
		}

		public void DFSInOrder(Node node)
		{
			if (node == null) return;

			if (node.Left != null) DFSInOrder(node.Left);
			Console.WriteLine(node.Value);
			if (node.Right != null) DFSInOrder(node.Right);
		}

		public void DFSInOrderNoRecursion()
		{
			if (this.Root == null) return;

			Stack<Node> stack = new Stack<Node>();

			Node temp = this.Root;
			while (temp != null)
			{
				stack.Push(temp);
				temp = temp.Left;
			}

			while (stack.Count > 0)
			{
				temp = stack.Pop();
				Console.WriteLine(temp.Value);

				if (temp.Right != null)
				{
					temp = temp.Right;
					while (temp != null)
					{
						stack.Push(temp);
						temp = temp.Left;
					}
				}
			}
		}

		public void DFSPostOrder()
		{
			DFSPostOrder(this.Root);
		}

		public void DFSPostOrder(Node node)
		{
			if (node == null) return;

			if (node.Left != null) DFSPostOrder(node.Left);
			if (node.Right != null) DFSPostOrder(node.Right);
			Console.WriteLine(node.Value);
		}

		public void DFSPostOrderNoRecursion()
		{
			if (this.Root == null) return;

			Stack<Node> s1 = new Stack<Node>();
			Stack<Node> s2 = new Stack<Node>();

			s1.Push(this.Root);

			while (s1.Count > 0)
			{
				Node temp = s1.Pop();
				s2.Push(temp);

				if (temp.Left != null) s1.Push(temp.Left);
				if (temp.Right != null) s1.Push(temp.Right);
			}

			while (s2.Count > 0)
			{
				Node temp = s2.Pop();
				Console.WriteLine(temp.Value);
			}
		}

		#endregion

		
	}
}
