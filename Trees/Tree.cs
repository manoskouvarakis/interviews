﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class Tree
	{
		public class Node
		{
			public int Value { get; set; }
			public Node[] Children { get; set; }
		}

		private Node Root { get; set; }
	}
}
