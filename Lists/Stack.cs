﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class Stack
	{
		public static void Push(ref StackElement stack, int data)
		{
			StackElement newElement = new StackElement(data);
			newElement.Next = stack;
			stack = newElement;
		}

		public static bool Pop(ref StackElement stack, out StackElement poppedElement)
		{
			poppedElement = stack;
			if (stack == null) return false;

			stack = stack.Next;
			return true;
		}

		public static void Print(StackElement stack)
		{
			while (stack != null)
			{
				Console.WriteLine(stack.IntData);
				stack = stack.Next;
			}
			Console.WriteLine("----------");
		}
	}

	public class StackElement
	{
		public StackElement Next { get; set; }
		public int IntData { get; set; }

		public StackElement(int data)
		{
			this.IntData = data;
		}
	}

	public class MyStack
	{
		public class MyStackElement
		{
			public MyStackElement Next { get; set; }
			public int IntData { get; set; }

			public MyStackElement(int data)
			{
				this.IntData = data;
			}
		}

		private MyStackElement Head { get; set; }

		public void Push(int data)
		{
			MyStackElement newElement = new MyStackElement(data);
			newElement.Next = this.Head;
			this.Head = newElement;
		}

		public MyStackElement Pop()
		{
			MyStackElement poppedElement = this.Head;
			this.Head = this.Head.Next;
			return poppedElement;
		}

		public void Print()
		{
			while (this.Head != null)
			{
				Console.WriteLine(this.Head.IntData);
				this.Head = this.Head.Next;
			}
			Console.WriteLine("----------");
		}
	}

}
