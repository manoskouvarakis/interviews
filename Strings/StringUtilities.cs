﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public static class StringUtilities
	{
		public static char FindFirstNonRepeated(String input)
		{
			if (String.IsNullOrEmpty(input)) return '\0';

			Dictionary<char, int> map = new Dictionary<char, int>();

			foreach (char c in input.ToCharArray())
			{
				if (!map.ContainsKey(c)) map.Add(c, 1);
				else map[c]++;
			}

			foreach (char c in input.ToCharArray())
			{
				if (map[c] == 1) return c;
			}

			return '\0';
		}


		public static String RemoveChars(String str, String remove)
		{
			HashSet<char> hash = new HashSet<char>();

			foreach (char c in remove.ToCharArray())
			{
				hash.Add(c);
			}

			//With string builder
			StringBuilder sb = new StringBuilder();
			foreach (char c in str.ToCharArray())
			{
				if (!hash.Contains(c)) sb.Append(c);
			}

			return sb.ToString();

			////without string builder
			//char[] s = str.ToCharArray();
			//int stringLength = str.Length;
			//int src = 0, des = 0;

			//while (src < stringLength)
			//{
			//	if (!map.ContainsKey(s[src]))
			//	{
			//		s[des] = s[src];
			//		des++;
			//	}
			//	src++;
			//}

			//return new string(s, 0, des);
		}


		public static String ReverseWords(String input)
		{
			char[] s = input.ToCharArray();
			int length = input.Length;

			int end = length - 1;

			StringBuilder sb = new StringBuilder();
			int start = end;
			while (start >= 0)
			{
				while (start >= 0 && s[start] != ' ') start--;

				for (int i = start + 1; i <= end; i++)
				{
					sb.Append(s[i]);
				}
				if (start >= 0) sb.Append(s[start]);

				start--;
				end = start;
			}

			return sb.ToString();
		}

		public static String ElegantReverseWords(String input)
		{
			char[] s = input.ToCharArray();
			int length = input.Length;

			int start = 0;
			int end = length - 1;

			StringUtilities.ReverseString(s, start, end);

			end = 0;

			while (end < length)
			{
				//if (s[end] == ' ')
				//{
				//	StringUtilities.ReverseString(s, start, end - 1);
				//	end++;
				//	start = end;
				//}
				//else
				//{
				//	end++;
				//}
				if (s[end] != ' ')
				{
					start = end;
					while (end < length && s[end] != ' ') end++;

					StringUtilities.ReverseString(s, start, end - 1);
				}
				end++;
			}

			//StringUtilities.ReverseString(s, start, end - 1);

			return new String(s);
		}

		private static void ReverseString(char[] word, int start, int end)
		{
			while (end > start)
			{
				char temp = word[start];
				word[start] = word[end];
				word[end] = temp;

				start++;
				end--;
			}
			//return word;
		}

		public static int StringToInt(String input)
		{
			int result = 0;
			bool isNegative = false;
			int pos = 0;

			if (input[pos] == '-')
			{
				isNegative = true;
				pos++;
			}

			while (pos < input.Length)
			{
				result *= 10;
				result += (input[pos] - '0');
				pos++;
			}

			if (isNegative) result = result * -1;

			return result;
		}

		public static String IntToString(int input)
		{
			bool isNegative = false;
			char value;

			StringBuilder sb = new StringBuilder();

			if (input < 0)
			{
				isNegative = true;
				input *= -1;
			}

			//while (input / 10 != 0)
			//{
			//	value = (char)((input % 10) + '0');
			//	sb.Append(value);
			//	input = input / 10;
			//}

			//value = (char)((input % 10) + '0');
			//sb.Append(value);

			while (input != 0)
			{
				value = (char)((input % 10) + '0');
				sb.Append(value);
				input = input / 10;
			}

			StringBuilder result = new StringBuilder();
			if (isNegative) result.Append('-');

			foreach (char a in sb.ToString().Reverse())
				result.Append(a);

			return result.ToString();
		}
	}
}
