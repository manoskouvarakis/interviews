﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	class Program
	{
		static void Main(string[] args)
		{
			#region List

			//ListElement head = new ListElement(1);
			//ListElement.InsertEnd(head, 2);
			//ListElement.InsertEnd(head, 3);
			//ListElement.InsertEnd(head, 4);
			//ListElement.InsertEnd(head, 5);
			//ListElement.InsertEnd(head, 6);

			//ListElement.Print(head);

			//bool isCycle = ListUtilities.IsCycle(head);

			////ListElement m = ListUtilities.MToLastElement(head, 4);
			////ListElement.Print(m);

			#endregion

			#region Stack

			////StackElement stack = null;

			////Stack.Push(ref stack, 1);
			////Stack.Push(ref stack, 2);
			////Stack.Push(ref stack, 3);

			////StackElement temp;
			////Stack.Pop(ref stack, out temp);

			////Stack.Push(ref stack, 4);
			////Stack.Push(ref stack, 5);

			////Stack.Print(stack);

			//MyStack stack = new MyStack();
			//stack.Push(1);
			//stack.Push(2);
			//stack.Push(3);
			//stack.Push(4);

			//stack.Pop();
			//stack.Pop();
			//stack.Print();

			#endregion

			#region Trees

			//BinaryTree bTree = new BinaryTree();

			//Console.WriteLine("-------PRE---------");
			//bTree.DFSPreOrder();
			//Console.WriteLine("----------------");
			//bTree.DFSPreOrderNoRecursion();
			//Console.WriteLine("-------IN------");
			//bTree.DFSInOrder();
			//Console.WriteLine("----------------");
			//bTree.DFSInOrderNoRecursion();
			//Console.WriteLine("------POST-------");
			//bTree.DFSPostOrder();
			//Console.WriteLine("----------------");
			//bTree.DFSPostOrderNoRecursion();

			//BinaryTree.Node a = bTree.GetLowestCommonAnsestor(4, 5);

			#endregion

			#region String and Arrays

			//Console.WriteLine(StringUtilities.FindFirstNonRepeated("awerawer"));

			//Console.WriteLine(StringUtilities.RemoveChars("awerawer", "ar"));

			Console.WriteLine(StringUtilities.ReverseWords("test test1. test2 t test3"));
			Console.WriteLine(StringUtilities.ElegantReverseWords("test test1. test2 t test3"));

			//Console.WriteLine(StringUtilities.StringToInt("0"));

			Console.WriteLine(StringUtilities.IntToString(53));

			#endregion

			#region Numbers

			//Console.WriteLine(Numbers.Factorial(5));
			//Console.WriteLine(Numbers.FactorialNoRecursion(5));

			//Console.WriteLine(Numbers.Fibonacci(43));
			//Console.WriteLine(Numbers.FibonacciOpt(43));
			//Console.WriteLine(Numbers.FibonacciNoRecursion(43));

			#endregion

			Console.ReadKey();
		}
	}

	public class Test
	{
		public int factorial(int i)
		{
			if (i < 0) return -1;
			if (i == 0) return 1;

			return i * factorial(i - 1);
		}
	}
}
